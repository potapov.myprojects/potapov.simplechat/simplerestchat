//package com.potapov.smilechat;
//
//import com.potapov.smilechat.util.JpaConfig;
//import com.potapov.smilechat.util.web.WebMvcConfig;
//import org.jetbrains.annotations.NotNull;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.web.WebApplicationInitializer;
//import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
//
//@EnableAutoConfiguration
//@ComponentScan(basePackages = "com.potapov.smilechat")
//public class AppInitializerOld extends AbstractAnnotationConfigDispatcherServletInitializer implements WebApplicationInitializer {
//    @NotNull private static final String DISPATCHER_SERVLET_NAME = "dispatcher";
//
//    @Override
//    protected Class<?>[] getRootConfigClasses() {
//        return new Class[] { JpaConfig.class };
//    }
//    @Override
//    protected Class<?>[] getServletConfigClasses() {
//        return new Class[] { WebMvcConfig.class };
//    }
//    @Override
//    protected String[] getServletMappings() {
//        return new String[] { "/" };
//    }
//
//    public static void main(String[] args) {
//        SpringApplication.run(AppInitializerOld.class, args);
//    }
//
////    @Override
////    public void onStartup(ServletContext servletContext) throws ServletException {
////
////        @NotNull AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
////        // регистрируем конфигурацию созданую высше
////        ctx.register(WebMvcConfig.class);
////        // добавляем в контекст слушателя с нашей конфигурацией
////        servletContext.addListener(new ContextLoaderListener(ctx));
////
////        ctx.setServletContext(servletContext);
////
////        // настраиваем маппинг Dispatcher Servlet-а
////        @NotNull ServletRegistration.Dynamic servlet =
////                servletContext.addServlet(DISPATCHER_SERVLET_NAME, new DispatcherServlet(ctx));
////        servlet.addMapping("/");
////        servlet.setLoadOnStartup(1);
////    }
//}
