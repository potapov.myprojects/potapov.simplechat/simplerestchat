package com.potapov.smilechat.service;

import com.potapov.smilechat.api.service.ICommonMessageService;
import com.potapov.smilechat.entity.message.CommonMessage;
import com.potapov.smilechat.service.abstarct.AbstractService;

public class CommonMessageService extends AbstractService<CommonMessage> implements ICommonMessageService {
}
