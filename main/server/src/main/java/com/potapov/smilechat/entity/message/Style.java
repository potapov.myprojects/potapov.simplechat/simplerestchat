package com.potapov.smilechat.entity.message;

import com.potapov.smilechat.api.entity.IAbstractEntity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "STYLE")
public class Style implements Cloneable, Serializable, IAbstractEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Style() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
