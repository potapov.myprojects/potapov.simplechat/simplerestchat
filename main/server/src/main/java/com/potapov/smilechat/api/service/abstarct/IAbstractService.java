package com.potapov.smilechat.api.service.abstarct;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAbstractService<T> {
    @NotNull Iterable<T> findAll();
    @Nullable T findById(@NotNull final Long id);
    @NotNull Long save(@NotNull T t);
}
