package com.potapov.smilechat.controller.user;

import com.potapov.smilechat.api.ServiceLocator;
import com.potapov.smilechat.dto.UserDto;
import com.potapov.smilechat.entity.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserRestController {
    @Autowired
    ServiceLocator serviceLocator;

    @GetMapping
    public ResponseEntity<Iterable<User>> getUserList(){
        @NotNull Iterable<User> users = serviceLocator.getUserService().findAll();
        return ResponseEntity.ok(users);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDto){
        @NotNull User user = userDto.toEntity();
        userDto.setId(serviceLocator.getUserService().save(user));
        return ResponseEntity.ok(userDto);
    }
}
