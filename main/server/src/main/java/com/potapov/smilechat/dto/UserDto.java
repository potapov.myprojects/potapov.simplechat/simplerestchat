package com.potapov.smilechat.dto;

import com.potapov.smilechat.api.ServiceLocator;
import com.potapov.smilechat.entity.User;
import com.potapov.smilechat.enumeration.RoleType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

public class UserDto {
    private @Nullable Long      id;
    private @Nullable String    login;
    private @Nullable String    hashPass;
    private @Nullable String    firstName;
    private @Nullable String    lastName;
    private @Nullable String    email;
    private @Nullable Date      birthDate;
    private @Nullable RoleType  roleType;

    @Autowired
    private @NotNull ServiceLocator serviceLocator;

    public UserDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHashPass() {
        return hashPass;
    }

    public void setHashPass(String hashPass) {
        this.hashPass = hashPass;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public @NotNull User toEntity(){
        User user = serviceLocator.getUserService().findById(id);
        if (user != null)
            return user;

        user = new User();
        user.setLogin(login);
        user.setHashPass(hashPass);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setRoleType(roleType);
        user.setBirthDate(birthDate);

        return user;
    }
}
