package com.potapov.smilechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.potapov.smilechat")
@EnableAutoConfiguration
public class AppInitializer  {
    public static void main(String[] args) {
        SpringApplication.run(AppInitializer.class, args);
    }
}
