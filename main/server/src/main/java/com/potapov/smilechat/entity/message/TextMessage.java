package com.potapov.smilechat.entity.message;

import com.potapov.smilechat.api.entity.IAbstractEntity;

import javax.persistence.*;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "TEXT_MESSAGE")
public class TextMessage implements Cloneable, Serializable, IAbstractEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    private String text;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn//(name="text_message_id")
    private Style style;


    public TextMessage() {
    }

    public TextMessage(String text) {
        this.text = text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public String getText() {
        return text;
    }

    public Style getStyle() {
        return style;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
