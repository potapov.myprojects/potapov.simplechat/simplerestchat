package com.potapov.smilechat.api;

import com.potapov.smilechat.api.service.IUserService;
import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {
    @NotNull IUserService getUserService();
}
