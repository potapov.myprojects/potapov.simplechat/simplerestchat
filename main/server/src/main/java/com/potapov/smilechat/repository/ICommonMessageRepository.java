package com.potapov.smilechat.repository;

import com.potapov.smilechat.entity.message.CommonMessage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICommonMessageRepository extends CrudRepository<CommonMessage, Long> {
}
