package com.potapov.smilechat.api.service;

import com.potapov.smilechat.entity.message.Style;
import com.potapov.smilechat.api.service.abstarct.IAbstractService;

public interface IStyleService extends IAbstractService<Style> {
}
