package com.potapov.smilechat.api.service;

import com.potapov.smilechat.entity.User;
import com.potapov.smilechat.api.service.abstarct.IAbstractService;

public interface IUserService extends IAbstractService<User> {
    void populateBaseWithUsers();
}
