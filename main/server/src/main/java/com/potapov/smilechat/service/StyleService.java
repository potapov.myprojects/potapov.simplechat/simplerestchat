package com.potapov.smilechat.service;

import com.potapov.smilechat.api.service.IStyleService;
import com.potapov.smilechat.entity.message.Style;
import com.potapov.smilechat.service.abstarct.AbstractService;

public class StyleService  extends AbstractService<Style> implements IStyleService {
}
