package com.potapov.smilechat.repository;

import com.potapov.smilechat.entity.message.Style;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IStyleRepository extends CrudRepository<Style, Long> {
}
