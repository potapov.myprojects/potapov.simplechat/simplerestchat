package com.potapov.smilechat.service;

import com.potapov.smilechat.api.service.ITextMessageService;
import com.potapov.smilechat.entity.message.TextMessage;
import com.potapov.smilechat.service.abstarct.AbstractService;

public class TextMessageService extends AbstractService<TextMessage> implements ITextMessageService {
}
