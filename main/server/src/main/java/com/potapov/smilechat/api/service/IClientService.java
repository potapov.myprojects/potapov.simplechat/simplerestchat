package com.potapov.smilechat.api.service;

import com.potapov.smilechat.entity.Client;
import com.potapov.smilechat.api.service.abstarct.IAbstractService;

public interface IClientService extends IAbstractService<Client> {
}
