package com.potapov.smilechat.service.abstarct;

import com.potapov.smilechat.api.entity.IAbstractEntity;
import com.potapov.smilechat.api.service.abstarct.IAbstractService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public abstract class AbstractService <T extends IAbstractEntity>  implements IAbstractService<T> {
    @Autowired
    @NotNull CrudRepository<T, Long> repository;

    public Iterable<T> findAll(){
        return repository.findAll();
    }

    @Override
    public @Nullable T findById(@NotNull Long id) {
        Optional<T> optionalUser  = repository.findById(id);
        return optionalUser.orElse(null);
    }

    @Override
    public @NotNull Long save(@NotNull T t) {
        repository.save(t);
        return t.getId();
    }
}
