package com.potapov.smilechat.api.entity;

import org.jetbrains.annotations.Nullable;

public interface IAbstractEntity {
    @Nullable Long getId();
}
