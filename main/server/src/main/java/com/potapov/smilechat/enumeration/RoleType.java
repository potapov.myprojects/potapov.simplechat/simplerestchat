package com.potapov.smilechat.enumeration;

public enum RoleType {
    Administrator,
    Manager,
    User;
}
