package com.potapov.smilechat.entity;

import com.potapov.smilechat.api.entity.IAbstractEntity;
import com.potapov.smilechat.dto.UserDto;
import com.potapov.smilechat.enumeration.RoleType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "USER")
public class User implements Cloneable, Serializable, IAbstractEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private @Nullable Long id;

    @Column(name = "LOGIN", nullable = false, unique = true)
    private @Nullable String login;

    @Column(name = "HASH_PASS")
    private @Nullable String hashPass;

    @Column(name = "FIRST_NAME")
    private @Nullable String firstName;

    @Column(name = "LAST_NAME")
    private @Nullable String lastName;

    @Column(name = "EMAIL")
    private @Nullable String email;

    @Column(name = "BIRTH_DATE")
    private @Nullable Date birthDate;

    @Column(name = "ROLE")
    @Enumerated(value = EnumType.STRING)
    private @NotNull RoleType roleType = RoleType.User;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHashPass() {
        return hashPass;
    }

    public void setHashPass(String hashPass) {
        this.hashPass = hashPass;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return "Contact - Id: " + id +
                ", Login: " + login +
                ", Pass: " + hashPass +
                ", First name: " + firstName +
                ", Last name: " + lastName +
                ", Role: " + roleType +
                ", Birthday: " + birthDate +
                ", E-mail: " + email;
    }

    public @NotNull UserDto toDto(){
        UserDto userDto = new UserDto();
        userDto.setId(id);
        userDto.setLogin(login);
        userDto.setHashPass(hashPass);
        userDto.setFirstName(firstName);
        userDto.setLastName(lastName);
        userDto.setRoleType(roleType);
        userDto.setBirthDate(birthDate);
        userDto.setEmail(email);

        return userDto;
    }
}
