package com.potapov.smilechat.entity.message;

import com.potapov.smilechat.api.entity.IAbstractEntity;

import javax.persistence.*;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "COMMON_MESSAGE")
public class CommonMessage implements Cloneable, Serializable, IAbstractEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn//(name="text_message_id")
    private TextMessage textMessage;

    public CommonMessage() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTextMessage(TextMessage textMessage) {
        this.textMessage = textMessage;
    }

    public TextMessage getTextMessage() {
        return textMessage;
    }
}
