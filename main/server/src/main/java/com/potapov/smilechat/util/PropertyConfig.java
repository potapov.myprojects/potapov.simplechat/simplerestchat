package com.potapov.smilechat.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:resources.properties")
@ComponentScan("com.potapov.smilechat")
public class PropertyConfig {

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource(@Value("${hibernate.connection.driver_class}") String driver){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(environment.getProperty("hibernate.connection.url"));
        dataSource.setUsername(environment.getProperty("hibernate.connection.username"));
        dataSource.setPassword(environment.getProperty("hibernate.connection.password"));

        return dataSource;
    }

    @Bean
    public Properties hibernateProperties() {
        return new Properties() {
            {
                setProperty("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto"));
                setProperty("hibernate.dialect",      environment.getProperty("hibernate.dialect"));
                setProperty("hibernate.show_sql",     environment.getProperty("hibernate.show_sql"));
//                setProperty("hibernate.globally__quoted__identifiers",  "true");
            }
        };
    }


}
