package com.potapov.smilechat.util.postprocess;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

@Component
public class PostProcessorBeanFactory implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
//        String[] names = configurableListableBeanFactory.getBeanDefinitionNames();
//        for (String name : names) {
//            System.out.println(name);
//        }
    }
}
