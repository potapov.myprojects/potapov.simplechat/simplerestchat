package com.potapov.smilechat.service;

import com.potapov.smilechat.api.service.IUserService;
import com.potapov.smilechat.entity.User;
import com.potapov.smilechat.enumeration.RoleType;
import com.potapov.smilechat.service.abstarct.AbstractService;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

@Service
public class UserService extends AbstractService<User> implements IUserService {

    @Override
    public void populateBaseWithUsers(){
        if (!findAll().iterator().hasNext()){
            createUser("admin", "admin", RoleType.Administrator);
            createUser("user", "user", RoleType.User);
        }
    }

    @NotNull
    public User createUser(@NotNull final String login,
                           @NotNull final String hashpass,
                           @NotNull final RoleType roleType){
        User user = new User();
        user.setLogin(login);
        user.setHashPass(hashpass);
        user.setRoleType(roleType);

        save(user);

        return user;
    }
}
