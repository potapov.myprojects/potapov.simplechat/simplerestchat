package com.potapov.smilechat.api.service;

import com.potapov.smilechat.entity.message.TextMessage;
import com.potapov.smilechat.api.service.abstarct.IAbstractService;

public interface ITextMessageService extends IAbstractService<TextMessage> {
}
