package com.potapov.smilechat.controller;

import com.potapov.smilechat.api.ServiceLocator;
import com.potapov.smilechat.api.service.IUserService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping(value = "/")
public class MainRestController implements ServiceLocator {

    @Autowired
    private @Nullable IUserService userService;

    public MainRestController() {}

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<Void> getRoot(){
        return ResponseEntity.noContent().build();
    }

    @PostConstruct
    private void init(){
        userService.populateBaseWithUsers();
    }
}
