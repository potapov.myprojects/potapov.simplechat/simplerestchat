package com.potapov.smilechat.api.service;

import com.potapov.smilechat.entity.message.CommonMessage;
import com.potapov.smilechat.api.service.abstarct.IAbstractService;

public interface ICommonMessageService extends IAbstractService<CommonMessage> {
}
